"user strict";
require("dotenv").config(); // include .env file

//local mysql db connection
const { Pool } = require("pg");
const poolV1 = new Pool({
    user: process.env.PGUSER,
    host: process.env.PGHOST,
    database: process.env.PGDATABASE,
    password: process.env.PGPASSWORD,
    port: process.env.PGPORT
});

module.exports = poolV1;