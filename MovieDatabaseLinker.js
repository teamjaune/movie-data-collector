const Db = require("./Utils/db");
const Logger = require("./Utils/Log");
const Csv = require("csvtojson");
const Format = require("pg-format");
const Axios = require('axios');

module.exports = {
    deleteMovie: async function (movieId) {
        await new Promise((resolve) => {
            const sqlDeleteMovie =
                `DELETE FROM movies WHERE movie_id = $1`;

            Db.query(sqlDeleteMovie,
                [movieId],
                (err) => {
                    if (err) {
                        Logger.error(err);
                        throw err;
                    }
                    resolve();
                });
        });
    },

    insertMovie: function (data) {
        return new Promise((resolve) => {
            if (data["titleType"] === "video" ||
                data["titleType"] === "tvEpisode" ||
                data["titleType"] === "videoGame" ||
                data["titleType"] === "radioEpisode" ||
                data["titleType"] === "tvPilot") {
                module.exports.deleteMovie(data["tconst"]).then(() => {
                    resolve(false);
                });
            } else {
                if (data["primaryTitle"] === "\\N" ||
                    data["originalTitle"] === "\\N" ||
                    data["isAdult"] === "\\N" ||
                    data["startYear"] === "\\N" ||
                    data["runtimeMinutes"] === "\\N" ||
                    data["titleType"] === "\\N") {
                    module.exports.deleteMovie(data["tconst"]).then(() => {
                        resolve(false);
                    });
                } else {
                    const movie = [];
                    movie.push(data["tconst"]);
                    movie.push(data["titleType"]);
                    movie.push(data["primaryTitle"].split("\\t")[0].split("\\N")[0]);
                    movie.push(data["originalTitle"].split("\\t")[0].split("\\N")[0]);
                    movie.push(data["isAdult"]);
                    movie.push(data["startYear"]);
                    movie.push(data["endYear"] === "\\N" ? null : data["endYear"]);
                    movie.push(data["runtimeMinutes"]);
                    const sqlUpdateMovie =
                        `UPDATE movies SET title_type = $2, primary_title = $3, original_title = $4, is_adult = $5, start_year = $6, end_year = $7, runtime_minutes = $8 WHERE movie_id = $1`;

                    Db.query(sqlUpdateMovie,
                        movie,
                        (err) => {
                            if (err) {
                                Logger.error(err);
                                throw err;
                            }
                            resolve(true);
                        });
                }
            }
        });
    },

    insertFrenchTranslation: async function (data) {
        await new Promise((resolve) => {
            const movie = [];
            movie.push(data["titleId"]);
            movie.push(data["title"]);
            const sqlUpdateMovie = `UPDATE movies SET french_title = $2 WHERE movie_id = $1`;

            Db.query(sqlUpdateMovie,
                movie,
                (err) => {
                    if (err) {
                        Logger.error(err);
                        throw err;
                    }
                    resolve();
                });
        });
    },

    insertGenres: async function (data) {
        await new Promise((resolve) => {
            const genres = data["genres"].split(",");
            const genresLinkedToMovie = [];
            for (let genre of genres) {
                if (genre !== "\\N") {
                    genresLinkedToMovie.push([data["tconst"], genre]);
                }
            }

            if (genresLinkedToMovie.length > 0) {
                const sqlInsertGenre =
                    Format("INSERT INTO genres (movie_id, genre) VALUES %L", genresLinkedToMovie);
                Db.query(sqlInsertGenre,
                    (err) => {
                        if (err) {
                            Logger.error(err);
                            throw err;
                        }
                        resolve();
                    });
            } else {
                module.exports.deleteMovie(data["tconst"]).then(() => {
                    resolve();
                });
            }
        });
    },

    insertRatings: function (data) {
        return new Promise((resolve) => {
            if (data["averageRating"] === "\\N" ||
                data["numVotes"] === "\\N") {
                resolve(false);
                return;
            }

            const sqlInsertRatings =
                "INSERT INTO movies (movie_id, average_rating, number_votes) VALUES ($1, $2, $3)";

            Db.query(sqlInsertRatings,
                [data["tconst"], data["averageRating"], data["numVotes"]],
                (err) => {
                    if (err) {
                        Logger.error(err);
                        throw err;
                    }
                    resolve(true);
                });
        });
    },

    insertDirectors: function (data) {
        return new Promise((resolve) => {
            const directors = data["directors"].split(",");

            const directorsLinkedToMovie = [];
            for (let director of directors) {
                if (director !== "\\N") {
                    directorsLinkedToMovie.push([data["tconst"], director, "director"]);
                }
            }

            if (directorsLinkedToMovie.length > 0) {
                const sqlInsertDirector = Format("INSERT INTO crew (movie_id, person_id, category) VALUES %L",
                    directorsLinkedToMovie);
                Db.query(sqlInsertDirector,
                    function (err) {
                        if (err) {
                            Logger.error(err);
                            throw err;
                        }
                        resolve(directors);
                    });
            } else {
                resolve([]);
            }
        });
    },

    insertWriters: function (data) {
        return new Promise((resolve) => {
            const writers = data["writers"].split(",");

            const writersLinkedToMovie = [];
            for (let writer of writers) {
                if (writer !== "\\N") {
                    writersLinkedToMovie.push([data["tconst"], writer, "writer"]);
                }
            }

            if (writersLinkedToMovie.length > 0) {
                const sqlInsertWriter =
                    Format("INSERT INTO crew (movie_id, person_id, category) VALUES %L", writersLinkedToMovie);
                Db.query(sqlInsertWriter,
                    (err) => {
                        if (err) {
                            Logger.error(err);
                            throw err;
                        }
                        resolve(writers);
                    });
            } else {
                resolve([]);
            }
        });
    },

    deleteCrew: async function (personId) {
        await new Promise((resolve) => {
            const sqlDeleteCrew =
                `DELETE FROM crew WHERE person_id = $1`;

            Db.query(sqlDeleteCrew,
                [personId],
                (err) => {
                    if (err) {
                        Logger.error(err);
                        throw err;
                    }
                    resolve();
                });
        });
    },

    insertPerson: async function (data) {
        await new Promise(async (resolve) => {

            if (data["primaryName"] === "\\N" || data["birthYear"] === "\\N") {
                resolve();
                return;
            }

            const deathYear = data["deathYear"] === "\\N" ? null : data["deathYear"];

            const person = [data["nconst"], data["primaryName"], data["birthYear"], deathYear];

            const sqlInsertPerson =
                "INSERT INTO person (person_id, name, birth_year, death_year) VALUES ($1, $2, $3, $4)";
            Db.query(sqlInsertPerson,
                person,
                (err) => {
                    if (err) {
                        Logger.error(err);
                        throw err;
                    }
                    resolve();
                });
        });
    },

    insertCrew: function (data) {
        return new Promise((resolve) => {
            if (data["category"] === "director" || data["category"] === "writer") {
                resolve(false);
                return;
            }
            const job = data["job"] === "\\N" ? null : data["job"].split("\\")[0].substring(0, 254);
            const crew = [data["tconst"], data["nconst"], data["category"], job];

            const sqlInsertCrew =
                "INSERT INTO crew (movie_id, person_id, category, job) VALUES ($1, $2, $3, $4) ON CONFLICT DO NOTHING";
            Db.query(sqlInsertCrew,
                crew,
                (err) => {
                    if (err) {
                        Logger.error(err);
                        throw err;
                    }
                    resolve(true);
                });
        });
    },

    getTimeElapsed: function (startTime) {
        const endTime = new Date();
        var timeDiff = endTime - startTime; //in ms
        // strip the ms
        timeDiff /= 1000;

        // get seconds 
        return Math.round(timeDiff);
    },

    importTitleBasics: async function (file, movies) {
        await new Promise((resolve) => {
            Logger.database(`Importing movies of ${movies.size} movies`);
            const startTime = new Date();
            Csv({
                delimiter: "\t"
            })
                .fromFile(file)
                .subscribe((data) => {
                    if (movies.has(data["tconst"])) {
                        return new Promise((finished) => {
                            module.exports.insertMovie(data).then((hasBeenInserted) => {
                                if (hasBeenInserted) {
                                    module.exports.insertGenres(data).then(() => {
                                        finished();
                                    });
                                } else {
                                    finished();
                                }
                            });
                        });
                    }
                },
                    (err) => {
                        Logger.error(err);
                        throw err;
                    },
                    () => {
                        new Promise((deleteAll) => {
                            const sqlDeleteMovies = `DELETE FROM movies WHERE original_title IS NULL`;
                            Db.query(sqlDeleteMovies,
                                (err) => {
                                    if (err) {
                                        Logger.error(err);
                                        throw err;
                                    }
                                    deleteAll();
                                });
                        }).then(() => {
                            Logger.success(`Movies importing done in ${module.exports.getTimeElapsed(startTime)} seconds`);
                            resolve();
                        });
                    });
        });
    },

    importTitleRatings: function (file) {
        return new Promise((resolve) => {
            Logger.database("Importing movies ratings");
            var movies = new Set();
            const startTime = new Date();
            Csv({
                delimiter: "\t"
            })
                .fromFile(file)
                .subscribe((data) => {
                    return new Promise((finished) => {
                        module.exports.insertRatings(data).then((hasBeenInserted) => {
                            if (hasBeenInserted) {
                                movies.add(data["tconst"]);
                            }
                            finished();
                        });
                    });
                },
                    (err) => {
                        Logger.error(err);
                        throw err;
                    },
                    () => {
                        Logger.success(
                            `Movies ratings importing done in ${module.exports.getTimeElapsed(startTime)} seconds`);
                        resolve(movies);
                    });
        });
    },

    importTitleCrew: function (file, movies) {
        return new Promise((resolve) => {
            Logger.database("Importing movies directors and writers");
            var persons = new Set();
            const startTime = new Date();
            Csv({
                delimiter: "\t"
            })
                .fromFile(file)
                .subscribe((data) => {
                    if (movies.has(data["tconst"])) {
                        return new Promise((finished) => {
                            module.exports.insertDirectors(data).then((directors) => {
                                for (let director of directors) {
                                    if (director !== "\\N" && !persons.has(director)) {
                                        persons.add(director);
                                    }
                                }
                                module.exports.insertWriters(data).then((writers) => {
                                    for (let writer of writers) {
                                        if (writer !== "\\N" && !persons.has(writer)) {
                                            persons.add(writer);
                                        }
                                    }
                                    finished();
                                });
                            });
                        });
                    }
                },
                    (err) => {
                        Logger.error(err);
                        throw err;
                    },
                    () => {
                        Logger.success(
                            `Movies Directors and Writers importing done in ${module.exports.getTimeElapsed(startTime)} seconds`);
                        resolve(persons);
                    });
        });
    },

    importNameBasics: async function (file, persons) {
        await new Promise((resolve) => {
            Logger.database("Importing persons");
            const startTime = new Date();
            Csv({
                delimiter: "\t"
            })
                .fromFile(file)
                .subscribe((data) => {
                    if (persons.has(data["nconst"])) {
                        return new Promise((finished) => {
                            module.exports.insertPerson(data).then(() => {
                                finished();
                            });
                        });
                    }
                },
                    (err) => {
                        Logger.error(err);
                        throw err;
                    },
                    () => {
                        Logger.success(`Persons importing done in ${module.exports.getTimeElapsed(startTime)} seconds`);
                        const startTimeCleaning = new Date();
                        new Promise((deleteAll) => {
                            const sqlDeleteCrew = `DELETE FROM crew WHERE NOT EXISTS (SELECT person_id FROM person WHERE person.person_id = crew.person_id)`;
                            Db.query(sqlDeleteCrew,
                                (err) => {
                                    if (err) {
                                        Logger.error(err);
                                        throw err;
                                    }
                                    deleteAll();
                                });
                        }).then(() => {
                            Logger.success(`Crew cleaning done in ${module.exports.getTimeElapsed(startTimeCleaning)} seconds`);
                            resolve();
                        });
                    });
        });
    },

    importTitlePrincipals: function (file, movies, persons) {
        return new Promise((resolve) => {
            Logger.database("Importing movies crew");
            const startTime = new Date();
            Csv({
                delimiter: "\t"
            })
                .fromFile(file)
                .subscribe((data) => {
                    if (movies.has(data["tconst"])) {
                        return new Promise((finished) => {
                            module.exports.insertCrew(data).then((hasBeenInserted) => {
                                if (hasBeenInserted) {
                                    if (!persons.has(data["nconst"])) {
                                        persons.add(data["nconst"]);
                                    }
                                }
                                finished();
                            });
                        });
                    }
                },
                    (err) => {
                        Logger.error(err);
                        throw err;
                    },
                    () => {
                        Logger.success(
                            `Movies crew importing done in ${module.exports.getTimeElapsed(startTime)} seconds`);
                        resolve(persons);
                    });
        });
    },

    importTitleAkas: async function (file, movies) {
        await new Promise((resolve) => {
            Logger.database("Importing french translation");
            const startTime = new Date();
            Csv({
                delimiter: "\t"
            })
                .fromFile(file)
                .subscribe((data) => {
                    if (movies.has(data["titleId"])) {
                        if (data["region"] === "FR") {
                            return new Promise((finished) => {
                                module.exports.insertFrenchTranslation(data).then(() => {
                                    finished();
                                });
                            });
                        }
                    }
                },
                    (err) => {
                        Logger.error(err);
                        throw err;
                    },
                    () => {
                        Logger.success(
                            `French translation importing done in ${module.exports.getTimeElapsed(startTime)} seconds`);
                        resolve();
                    });
        });
    },

    getAllMoviesID: function () {
        return new Promise((resolve) => {
            const sqlSelectMovie = `SELECT movie_id FROM movies`;

            Db.query(sqlSelectMovie,
                (err, res) => {
                    if (err) {
                        Logger.error(err);
                        throw err;
                    }
                    let movies = new Set();
                    for (let movie of res.rows) {
                        if (!movies.has(movie["movie_id"])) {
                            movies.add(movie["movie_id"]);
                        }
                    }
                    resolve(movies);
                });
        });
    },

    getSynopsisFromTMDB: async function (id) {
        return new Promise((resolve) => {
            Axios.get(`https://api.themoviedb.org/3/find/${id}?api_key=${process.env.TMDB_API_KEY}&external_source=imdb_id`)
                .then(function (response) {
                    if (response.data.movie_results.length > 0) {
                        if (response.data.movie_results[0].overview) {
                            resolve(`${response.data.movie_results[0].overview}`);
                        }
                    }
                    if (response.data.tv_results.length > 0) {
                        if (response.data.tv_results[0].overview) {
                            resolve(`${response.data.tv_results[0].overview}`);
                        }
                    }
                    if (response.data.tv_episode_results.length > 0) {
                        if (response.data.tv_episode_results[0].overview) {
                            resolve(`${response.data.tv_episode_results[0].overview}`);
                        }
                    }
                    if (response.data.tv_season_results.length > 0) {
                        if (response.data.tv_season_results[0].overview) {
                            resolve(`${response.data.tv_season_results[0].overview}`);
                        }
                    }
                    resolve(null);
                })
                .catch(function (error) {
                    Logger.error(error);
                    resolve(null);
                });
        });
    },

    updateSynopsisForMovie: async function (movieId) {
        let synopsis = await module.exports.getSynopsisFromTMDB(movieId);
        if (synopsis) {
            synopsis = synopsis.replace(/'/g, "''");
            const sql = `
            UPDATE movies SET
            synopsis = '${synopsis}' WHERE
            movie_id = '${movieId}'`;
            await new Promise((resolve) => {
                Db.query(sql,
                    function (err) {
                        if (err) {
                            Logger.error(`Error Inserting synopsis for movie ${movieId}`);
                            Logger.error(err);
                        }
                        resolve();
                    });
            });
        }
    },

    getAllSynopsis: async function (allMovies) {
        for (let movie of allMovies) {
            await module.exports.updateSynopsisForMovie(movie);
        }
    },

    storeIntoDatabase: async function () {

        module.exports.importTitleRatings("./Data/title.ratings.tsv").then((movies) => {
            module.exports.importTitleBasics("./Data/title.basics.tsv", movies).then(() => {
                module.exports.getAllMoviesID().then((allMovies) => {
                    module.exports.getAllSynopsis(allMovies);
                    module.exports.importTitleAkas("./Data/title.akas.tsv", allMovies);
                    module.exports.importTitleCrew("./Data/title.crew.tsv", allMovies).then((persons) => {
                        module.exports.importTitlePrincipals("./Data/title.principals.tsv", allMovies, persons).then(
                            (allPersons) => {
                                module.exports.importNameBasics("./Data/name.basics.tsv", allPersons);
                            });
                    });
                });
            });
        });
    }
};