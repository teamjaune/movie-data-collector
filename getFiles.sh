#!/bin/sh
cd ./Data/

echo "Downloading name.basics.tsv.gz"
curl -O https://datasets.imdbws.com/name.basics.tsv.gz 
echo "Unzipping name.basics.tsv.gz"
gunzip name.basics.tsv.gz
rm name.basics.tsv.gz

echo "Downloading title.akas.tsv.gz"
curl -O https://datasets.imdbws.com/title.akas.tsv.gz 
echo "Unzipping title.akas.tsv.gz"
gunzip title.akas.tsv.gz
rm title.akas.tsv.gz

echo "Downloading title.basics.tsv.gz"
curl -O https://datasets.imdbws.com/title.basics.tsv.gz 
echo "Unzipping title.basics.tsv.gz"
gunzip title.basics.tsv.gz
rm title.basics.tsv.gz

echo "Downloading title.crew.tsv.gz"
curl -O https://datasets.imdbws.com/title.crew.tsv.gz 
echo "Unzipping title.crew.tsv.gz"
gunzip title.crew.tsv.gz
rm title.crew.tsv.gz

echo "Downloading title.episode.tsv.gz"
curl -O https://datasets.imdbws.com/title.episode.tsv.gz 
echo "Unzipping title.episode.tsv.gz"
gunzip title.episode.tsv.gz
rm title.episode.tsv.gz

echo "Downloading title.principals.tsv.gz"
curl -O https://datasets.imdbws.com/title.principals.tsv.gz
echo "Unzipping title.principals.tsv.gz"
gunzip title.principals.tsv.gz
rm title.principals.tsv.gz

echo "Downloading title.ratings.tsv.gz"
curl -O https://datasets.imdbws.com/title.ratings.tsv.gz 
echo "Unzipping title.ratings.tsv.gz"
gunzip title.ratings.tsv.gz
rm title.ratings.tsv.gz

